# CONCEPT:
---
## Scope:
- Requirements necessary to complete this project in compliance with UK Emergency Ventilator Standards

## Team Lead(s):
|GitLab|Slack|
|---|---|
|  |  |
|  |  |

## Slack Channels:
-

## Issue Labels:
- Legal Requirements
- Technical Requirements

## Current Collab Docs:
- [Requirements Index](requirements/index.md)

## Overview:


## References:
- [UK RMV Requirements](requirements/UK_Rapidly-Manufactured_Vent_Requirements.pdf)
